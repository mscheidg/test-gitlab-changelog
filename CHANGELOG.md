## v1.0.1 (2023-01-09)

### improvement (2 changes)

- [Improved ci with changelog generation trigger.](mscheidg/test-gitlab-changelog@6bf1d07034b39fdfbae7c1b853da7103ae9475c0)
- [Test commit. Short.](mscheidg/test-gitlab-changelog@c5d2e835a28c2c64aadba13500d4017bc7a18062)

### fixed (3 changes)

- [Fix in MR squash commit message. short.](mscheidg/test-gitlab-changelog@6713ab001e5a988d998a8754ad19e3d74184c414) ([merge request](mscheidg/test-gitlab-changelog!2))
- [Fix commit in branch, no squash, short.](mscheidg/test-gitlab-changelog@b6038119f345a2b503f601371a767419c0426961)
- [Plain fix commit main branch. Short.](mscheidg/test-gitlab-changelog@2e1d3d72ebd24f01b0368431e064754b75a700d4)
## 1.0.3 (2023-01-09)

No changes.

## 1.0.2 (2023-01-09)

### improvement (1 change)

- [Update ci.](mscheidg/test-gitlab-changelog@03a591eb61deaa3d6ee3e5cd45a14c88836839c5)
